ruby-fugit (1.8.1-3) unstable; urgency=medium

  * Team upload.
  * Reupload to unstable

  [ Vinay Keshava ]
  * add experimental variable in d/salsa-ci.yml

  [ Pirate Praveen ]
  * Add ruby-concurrent (>= 1.1.6+dfsg-5~) as build dependency
  * Drop obsolete X{S,B}-Ruby-Versions fields

 -- Pirate Praveen <praveen@debian.org>  Tue, 13 Jun 2023 12:18:42 +0530

ruby-fugit (1.8.1-2) experimental; urgency=medium

  * Team upload.
  * Rebuild and update minimum version of ruby-et-orbi

 -- Vinay Keshava <vinaykeshava@disroot.org>  Mon, 24 Apr 2023 23:34:10 +0530

ruby-fugit (1.8.1-1) experimental; urgency=medium


  * Team upload.
  * Update standards version to 4.6.2, no changes needed.
  * New upstream version 1.8.1

 -- Vinay Keshava <vinaykeshava@disroot.org>  Mon, 20 Mar 2023 14:52:52 +0530

ruby-fugit (1.5.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * d/control (Standards-Version): Bump to 4.6.0.
    (Build-Depends): Bump versions for ruby-raabro.
    (Depends): Handle dependencies with ${ruby:Depends}.
  * d/copyright (Copyright): Update and add team.
  * d/rules: Install upstream changelog.
  * d/watch: Update file.
  * d/upstream/metadata: Update Changelog URL.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 23 Nov 2021 20:30:20 +0100

ruby-fugit (1.3.8-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ Cédric Boutillier ]
  * [ci skip] Update team name
  * New upstream version 1.3.8
  * Add upstream metadata and .gitattributes, refresh test runner
  * Gem install layout
  * Refresh control file
  * Bump ruby-raabro dependency min version to 1.3

 -- Cédric Boutillier <boutil@debian.org>  Thu, 03 Sep 2020 21:21:19 +0200

ruby-fugit (1.3.3+gh-1) unstable; urgency=medium

  * Team upload
  * Use Github as source to fetch tests
  * Build-depend on rspec and chronic to run the tests
  * Bump dependency version on ruby-et-orbi to >= 1.1.8
  * Bump Standards-Version to 4.4.0 (no changes needed)
  * Drop compat file, rely on debhelper-compat and bump compat level to 12

 -- Cédric Boutillier <boutil@debian.org>  Tue, 10 Sep 2019 11:47:56 +0200

ruby-fugit (1.3.3-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Pirate Praveen ]
  * Fix changelog entries (previous change was not uploaded)

  [ Sruthi Chandran ]
  * New upstream version 1.3.3

 -- Sruthi Chandran <srud@debian.org>  Thu, 05 Sep 2019 21:19:09 +0530

ruby-fugit (1.1.8-1) unstable; urgency=medium

  * Team upload

  [ Utkarsh Gupta ]
  * New upstream version 1.1.8
  * Add d/upstream/metadata

  [ Pirate Praveen ]
  * Tighten dependency on ruby-et-orbi

 -- Pirate Praveen <praveen@debian.org>  Wed, 06 Mar 2019 13:44:55 +0530

ruby-fugit (1.1.7-1) unstable; urgency=medium

  * New upstream version 1.1.7
  * Bump Standards-Version to 4.3.0 (no changes needed)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 16 Jan 2019 07:20:21 +0530

ruby-fugit (1.1.6-1) unstable; urgency=medium

  * Initial release (Closes: #916871)

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Thu, 20 Dec 2018 01:03:13 +0530
